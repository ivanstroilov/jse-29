package ru.t1.stroilov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.component.Server;

import java.net.ServerSocket;
import java.net.Socket;

public class ServerAcceptTask extends AbstractServiceTask {

    public ServerAcceptTask(final @NotNull Server server) {
        super(server);
    }

    @Override
    @SneakyThrows
    public void run() {
        @Nullable final ServerSocket serverSocket = server.getServerSocket();
        if (serverSocket == null) return;
        @NotNull final Socket socket = serverSocket.accept();
        server.submit(new ServerRequestTask(server, socket));
        server.submit(new ServerAcceptTask(server));
    }

}
