package ru.t1.stroilov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Contract(pure = true)
    protected Predicate<Task> filterByProjectId(@NotNull final String projectId) {
        return m -> projectId.equals(m.getId());
    }

    @Nullable
    @Override
    public Task add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public Task add(@Nullable final String userId, @Nullable final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    public List<Task> findAllByProjectID(@NotNull final String userId, @NotNull final String projectId) {
        return findAll().stream().filter(filterByUserId(userId)).filter(filterByProjectId(projectId)).collect(Collectors.toList());
    }

}
