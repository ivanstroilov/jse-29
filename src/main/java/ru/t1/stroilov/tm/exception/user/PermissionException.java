package ru.t1.stroilov.tm.exception.user;

import ru.t1.stroilov.tm.exception.system.AbstractSystemException;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Permission denied...");
    }

}
