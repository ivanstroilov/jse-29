package ru.t1.stroilov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.endpoint.IOperation;
import ru.t1.stroilov.tm.dto.request.AbstractRequest;
import ru.t1.stroilov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, IOperation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final IOperation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final IOperation operation = map.get(request.getClass());
        if (operation == null) throw new RuntimeException(request.getClass().getName());
        return operation.execute(request);
    }

}
