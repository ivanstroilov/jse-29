package ru.t1.stroilov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter

public class Command {

    @NotNull
    private String argument;

    @NotNull
    private String description;

    @NotNull
    private String name;

    public Command() {
    }

    public Command(@NotNull final String name, @NotNull final String argument, @NotNull final String description) {
        setName(name);
        setArgument(argument);
        setDescription(description);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}