package ru.t1.stroilov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.model.ICommand;
import ru.t1.stroilov.tm.api.service.IAuthService;
import ru.t1.stroilov.tm.api.service.IPropertyService;
import ru.t1.stroilov.tm.api.service.IServiceLocator;
import ru.t1.stroilov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    @NotNull
    protected IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

    public abstract Role[] getRoles();

    @NotNull
    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    public String getUserId() {
        return getAuthService().getUserId();
    }

}
