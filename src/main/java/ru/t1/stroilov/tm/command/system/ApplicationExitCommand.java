package ru.t1.stroilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    public final static String DESCRIPTION = "Terminate the application.";

    @NotNull
    public final static String NAME = "exit";

    @NotNull
    public final static String ARGUMENT = null;

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
