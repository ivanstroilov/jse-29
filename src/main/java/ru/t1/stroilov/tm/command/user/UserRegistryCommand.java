package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.enumerated.Role;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "Register User.";

    @NotNull
    public final static String NAME = "user-registry";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER Registry]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
