package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Project;

@NoArgsConstructor
public class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(final @Nullable Project project) {
        super(project);
    }

}
