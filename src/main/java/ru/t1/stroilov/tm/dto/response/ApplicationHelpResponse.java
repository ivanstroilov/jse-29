package ru.t1.stroilov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class ApplicationHelpResponse extends AbstractResponse {

    @Nullable
    private List<String> commands;

}
