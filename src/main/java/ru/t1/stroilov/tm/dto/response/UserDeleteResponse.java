package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.User;

@NoArgsConstructor
public class UserDeleteResponse extends AbstractUserResponse {

    public UserDeleteResponse(final @Nullable User user) {
        super(user);
    }

}
