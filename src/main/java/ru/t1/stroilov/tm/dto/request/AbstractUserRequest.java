package ru.t1.stroilov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractUserRequest extends AbstractRequest {

    @Nullable
    private String userId;

}
