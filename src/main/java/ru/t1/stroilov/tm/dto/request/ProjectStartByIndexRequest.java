package ru.t1.stroilov.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectStartByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

}
