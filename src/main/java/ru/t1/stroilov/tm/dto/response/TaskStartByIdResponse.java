package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Task;

@NoArgsConstructor
public class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(final @Nullable Task task) {
        super(task);
    }

}
