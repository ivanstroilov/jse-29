package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResponse {

}
