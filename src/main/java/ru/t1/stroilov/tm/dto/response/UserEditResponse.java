package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.User;

@NoArgsConstructor
public class UserEditResponse extends AbstractUserResponse {

    public UserEditResponse(final @Nullable User user) {
        super(user);
    }

}
