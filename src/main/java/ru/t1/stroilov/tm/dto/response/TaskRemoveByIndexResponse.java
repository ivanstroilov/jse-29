package ru.t1.stroilov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Task;

@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(final @Nullable Task task) {
        super(task);
    }

}
