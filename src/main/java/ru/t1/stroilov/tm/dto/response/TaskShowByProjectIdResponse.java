package ru.t1.stroilov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

@Getter
@Setter
public class TaskShowByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskShowByProjectIdResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

}
