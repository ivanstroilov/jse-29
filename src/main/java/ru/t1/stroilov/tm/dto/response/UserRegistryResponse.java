package ru.t1.stroilov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.User;

public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
