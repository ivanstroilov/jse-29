package ru.t1.stroilov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.IRepository;
import ru.t1.stroilov.tm.api.service.IService;
import ru.t1.stroilov.tm.exception.field.IdEmptyException;
import ru.t1.stroilov.tm.exception.field.IndexIncorrectException;
import ru.t1.stroilov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteAll(@NotNull final Collection<M> collection) {
        repository.deleteAll(collection);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        return repository.set(models);
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public M findByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Nullable
    @Override
    public M delete(@Nullable M model) {
        if (model == null) return null;
        return repository.delete(model);
    }

    @Nullable
    @Override
    public M deleteById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.deleteById(id);
    }

    @Nullable
    @Override
    public M deleteByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return repository.deleteByIndex(index);
    }

    @NotNull
    @Override
    public int getSize() {
        return repository.getSize();
    }
}
