package ru.t1.stroilov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractClient() {
    }

    public AbstractClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    protected Object call(@NotNull final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(socket.getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(socket.getInputStream());
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

}

