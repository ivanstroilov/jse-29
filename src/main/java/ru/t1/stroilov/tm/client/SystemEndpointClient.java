package ru.t1.stroilov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.stroilov.tm.dto.request.ServerAboutRequest;
import ru.t1.stroilov.tm.dto.request.ServerVersionRequest;
import ru.t1.stroilov.tm.dto.response.ServerAboutResponse;
import ru.t1.stroilov.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractClient implements ISystemEndpoint {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}

