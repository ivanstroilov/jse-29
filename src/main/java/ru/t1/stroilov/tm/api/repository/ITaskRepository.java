package ru.t1.stroilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    Task add(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task add(@Nullable String userId, @Nullable String name);

    @NotNull
    List<Task> findAllByProjectID(@NotNull String userId, @NotNull String projectId);
}
