package ru.t1.stroilov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    Integer getServerPort();

}
