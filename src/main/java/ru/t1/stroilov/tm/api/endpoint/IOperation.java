package ru.t1.stroilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.dto.request.AbstractRequest;
import ru.t1.stroilov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}

