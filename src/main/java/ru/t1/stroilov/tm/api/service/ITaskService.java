package ru.t1.stroilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.api.repository.ITaskRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectID(@Nullable String userId, @Nullable String projectId);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

}
